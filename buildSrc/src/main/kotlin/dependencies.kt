package org.ekans.kimple.buildsrc

object Modules {
    const val DOMAIN = ":modules:domain"
    const val DATA = ":modules:data"
    const val SERVER = ":modules:server"
    const val SERVER_API = ":modules:server-api"
}

object Versions {
    const val JVM_TARGET = "1.8"
}

object Librairies {

    object Versions {
        const val COROUTINES_CORE = "1.1.1"
        const val HOCON_CONFIG = "1.3.2"
        const val JACKSON = "2.9.8"
        const val KOIN = "1.0.2"
        const val LOGBACK_CLASSIC = "1.2.1"
        const val VERTX = "3.6.3"
    }

    const val COROUTINES_CORE = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.COROUTINES_CORE}"
    const val KOIN_CORE = "org.koin:koin-core:${Versions.KOIN}"
    const val HOCON_CONFIG = "com.typesafe:config:${Versions.HOCON_CONFIG}"
    const val JACKSON_MODULE_KOTLIN = "com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.JACKSON}"
    const val KOTLIN_STDLIB = "org.jetbrains.kotlin:kotlin-stdlib:${Plugin.Versions.KOTLIN}"
    const val LOGBACK_CLASSIC = "ch.qos.logback:logback-classic:${Versions.LOGBACK_CLASSIC}"
    const val VERTX_CORE = "io.vertx:vertx-core:${Versions.VERTX}"
    const val VERTX_LANG_KOTLIN = "io.vertx:vertx-lang-kotlin:${Versions.VERTX}"
    const val VERTX_KOTLIN_COROUTINES = "io.vertx:vertx-lang-kotlin-coroutines:${Versions.VERTX}"
    const val VERTX_WEB = "io.vertx:vertx-web:${Versions.VERTX}"
}
