interface Plugin {
    object Versions {
        const val BEN_NAMES_VERSIONS = "0.20.0"
        const val KOTLIN = "1.3.11"
        const val VERTX = "0.3.1"
    }
}