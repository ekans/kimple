import org.ekans.kimple.buildsrc.Librairies
import org.ekans.kimple.buildsrc.Modules
import org.ekans.kimple.buildsrc.Versions
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

dependencies {
    implementation(project(Modules.DOMAIN))
    implementation(Librairies.KOTLIN_STDLIB)
}

tasks {
    withType(KotlinCompile::class) {
        kotlinOptions.jvmTarget = Versions.JVM_TARGET
    }
}
