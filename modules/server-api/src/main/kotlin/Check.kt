package org.ekans.kimple.server.api

data class Check(val status: Status)
enum class Status {
    UP,
    DOWN
}