package org.ekans.kimple.server.api

typealias NoteContent = String

data class Note(val id: String?, val content: NoteContent)
