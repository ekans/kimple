package org.ekans.kimple.server.api

data class Error(val message: String)