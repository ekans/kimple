package org.ekans.kimple.domain.entities

data class Note(val id: String?, val content: String)
