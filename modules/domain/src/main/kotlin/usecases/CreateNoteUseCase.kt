package org.ekans.kimple.domain.usecases

import org.ekans.kimple.domain.entities.Note

interface CreateNoteUseCase {

  fun createNote(note: Note): Note
}
