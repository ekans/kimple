package org.ekans.kimple.domain.usecases

import org.ekans.kimple.domain.entities.Note

interface GetNotesUseCase {

  fun getNotes(): List<Note>
}