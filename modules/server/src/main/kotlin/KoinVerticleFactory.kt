package org.ekans.kimple.server

import io.vertx.core.Verticle
import io.vertx.core.spi.VerticleFactory
import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.koin.standalone.KoinComponent
import org.koin.standalone.get
import kotlin.reflect.KClass

object KoinVerticleFactory : VerticleFactory, KoinComponent {
    override fun prefix(): String = "koin"

    override fun createVerticle(verticleName: String, classLoader: ClassLoader): CoroutineVerticle {
        return get(clazz = Class.forName(verticleName.substringAfter("koin:")).kotlin)
    }
}

inline fun <reified T> KClass<T>.koinName() where T : Verticle =
    "${KoinVerticleFactory.prefix()}:${this.java.canonicalName}"