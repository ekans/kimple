package org.ekans.kimple.server

import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.serviceproxy.ServiceBinder
import org.ekans.kimple.server.services.DataService
import org.ekans.kimple.server.services.DataServiceFactory
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(DataVerticle::class.java)

class DataVerticle : CoroutineVerticle() {

  override suspend fun start() {
    logger.info("starting...")

    ServiceBinder(vertx)
      .setAddress(DataServiceFactory.address)
      .register(DataService::class.java, DataServiceFactory.create())

    logger.info("started")
  }

  override suspend fun stop() {
    logger.info("stopped")
  }
}
