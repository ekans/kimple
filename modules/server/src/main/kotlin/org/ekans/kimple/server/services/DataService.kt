package org.ekans.kimple.server.services

import io.vertx.codegen.annotations.ProxyGen
import io.vertx.codegen.annotations.VertxGen
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import org.ekans.kimple.domain.usecases.CreateNoteUseCase
import org.ekans.kimple.domain.usecases.GetNotesUseCase
import org.koin.standalone.KoinComponent
import org.koin.standalone.get

@ProxyGen
@VertxGen
interface DataService {

  fun getNotes(handler: Handler<AsyncResult<List<NoteMessage>>>)
  fun createNote(noteMessage: NoteMessage, handler: Handler<AsyncResult<NoteMessage>>)
}

object DataServiceFactory {
  const val address = "data-service"
  fun create(): DataService {
    return DataServiceImpl()
  }

  fun createProxy(vertx: Vertx): DataService {
    return DataServiceVertxEBProxy(vertx, address)
  }
}

private class DataServiceImpl : DataService, KoinComponent {

  private val getNotesUseCase = get<GetNotesUseCase>()
  private val createNoteUseCase = get<CreateNoteUseCase>()

  override fun getNotes(handler: Handler<AsyncResult<List<NoteMessage>>>) {

    try {
      val notes = getNotesUseCase
        .getNotes()
        .map {
          val jsonObject = JsonObject.mapFrom(it)
          NoteMessage(jsonObject)
        }

      handler.handle(Future.succeededFuture(notes))

    } catch (e: Throwable) {
      handler.handle(Future.failedFuture(e))
    }
  }

  override fun createNote(noteMessage: NoteMessage, handler: Handler<AsyncResult<NoteMessage>>) {

    try {
      val note = createNoteUseCase.createNote(noteMessage.toDomain())
      val jsonObject = JsonObject.mapFrom(note)
      val message = NoteMessage(jsonObject)

      handler.handle(Future.succeededFuture(message))

    } catch (e: Throwable) {
      handler.handle(Future.failedFuture(e))
    }
  }
}