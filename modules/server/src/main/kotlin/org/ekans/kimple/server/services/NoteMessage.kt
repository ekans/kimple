package org.ekans.kimple.server.services

import io.vertx.codegen.annotations.DataObject
import io.vertx.core.json.JsonObject
import org.ekans.kimple.domain.entities.Note

@DataObject(generateConverter = true)
class NoteMessage(json: JsonObject) {

  val id: String? = json.getString("id")
  val content: String = json.getString("content")

  companion object {
    fun fromApi(note: org.ekans.kimple.server.api.Note): NoteMessage {

      val json = JsonObject.mapFrom(note)
      return NoteMessage(json)
    }
  }

  fun toJson(): JsonObject = JsonObject.mapFrom(this)
  fun toDomain() = Note(id = id, content = content)
  fun toApi() = org.ekans.kimple.server.api.Note(id = id, content = content)
}
