package org.ekans.kimple.server.handlers

import com.fasterxml.jackson.databind.ObjectMapper
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import org.ekans.kimple.server.services.DataService
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import org.slf4j.LoggerFactory

object GetNotes : Handler<RoutingContext>, KoinComponent {
  private val logger = LoggerFactory.getLogger(this::class.java)

  private val mapper by inject<ObjectMapper>()
  private val dataService by inject<DataService>()

  override fun handle(ctx: RoutingContext) {
    logger.debug("GET /notes")

    dataService.getNotes(Handler {

      if (it.succeeded()) {
        val notes = it.result()
          .map { note -> note.toApi() }

        val json = mapper.writeValueAsString(notes)
        ctx.response().json(json)

      } else {
        ctx.fail(500, it.cause())
      }
    })
  }
}