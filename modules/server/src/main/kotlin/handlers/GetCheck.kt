package org.ekans.kimple.server.handlers

import com.fasterxml.jackson.databind.ObjectMapper
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import org.ekans.kimple.server.api.Check
import org.ekans.kimple.server.api.Status
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import org.slf4j.LoggerFactory

object GetCheck : Handler<RoutingContext>, KoinComponent {
  private val logger = LoggerFactory.getLogger(this::class.java)

  private val mapper by inject<ObjectMapper>()

  override fun handle(ctx: RoutingContext) {
    logger.debug("GET /check")

    val up = Check(Status.UP)
    val json = mapper.writeValueAsString(up)
    ctx.response().json(json)
  }
}