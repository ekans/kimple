package org.ekans.kimple.server.handlers

import com.fasterxml.jackson.databind.ObjectMapper
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import org.ekans.kimple.server.api.Error
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import org.slf4j.LoggerFactory

object Failure : Handler<RoutingContext>, KoinComponent {
  private val logger = LoggerFactory.getLogger(this::class.java)

  private val mapper by inject<ObjectMapper>()

  override fun handle(ctx: RoutingContext) {
    logger.debug("Handle failure")

    val message = ctx.failure().message
    logger.error(message, ctx.failure())

    val error = Error(message.toString())
    val response = ctx.response()
    val json = mapper.writeValueAsString(error)
    response.json(json, statusCode = ctx.statusCode())
  }
}