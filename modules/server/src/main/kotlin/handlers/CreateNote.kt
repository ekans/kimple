package org.ekans.kimple.server.handlers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import org.ekans.kimple.server.api.Note
import org.ekans.kimple.server.services.DataService
import org.ekans.kimple.server.services.NoteMessage
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import org.slf4j.LoggerFactory

object CreateNote : Handler<RoutingContext>, KoinComponent {
  private val logger = LoggerFactory.getLogger(this::class.java)

  private val mapper by inject<ObjectMapper>()
  private val dataService by inject<DataService>()

  override fun handle(ctx: RoutingContext) {

    val body = ctx.bodyAsString
    logger.debug("POST /notes body[$body]")

    val note: Note = jacksonObjectMapper()
      .readValue(body, Note::class.java)
    val message = NoteMessage.fromApi(note)

    dataService.createNote(message, Handler {
      if (it.succeeded()) {
        val created = it.result()
        val json = mapper.writeValueAsString(created.toApi())
        ctx.response().json(json)

      } else {
        ctx.fail(500, it.cause())
      }
    })
  }
}