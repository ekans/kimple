package org.ekans.kimple.server.handlers

import io.vertx.core.http.HttpServerResponse
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler

fun Router.installHandlers() {

  get("/check").handler(GetCheck)
  get("/notes").handler(GetNotes)

  route("/notes").handler(BodyHandler.create())
  post("/notes").handler(CreateNote)

  route().failureHandler(Failure)
}

fun HttpServerResponse.json(json: String, statusCode: Int = 200) {

  this.putHeader("content-type", "application/json")
  this.statusCode = statusCode
  this.end(json)
}