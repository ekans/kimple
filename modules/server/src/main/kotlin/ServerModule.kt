package org.ekans.kimple.server

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.vertx.core.Vertx
import io.vertx.ext.web.Router
import org.ekans.kimple.server.services.DataServiceFactory
import org.koin.dsl.module.module

val serverModule = module {
  single { getProperty("vertx-instance") as Vertx }
  single { jacksonObjectMapper() }
  single { DataServiceFactory.createProxy(get()) }
  single { Router.router(get()) }
  factory { DataVerticle() }
  factory { HttpServerVerticle() }
}
