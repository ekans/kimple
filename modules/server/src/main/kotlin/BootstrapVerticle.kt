package org.ekans.kimple.server

import io.vertx.kotlin.core.closeAwait
import io.vertx.kotlin.core.deployVerticleAwait
import io.vertx.kotlin.coroutines.CoroutineVerticle
import kotlinx.coroutines.runBlocking
import org.ekans.kimple.data.dataModule
import org.koin.standalone.KoinComponent
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.setProperty
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(BootstrapVerticle::class.java)

class BootstrapVerticle : CoroutineVerticle(), KoinComponent {

  override suspend fun start() {
    logger.info("starting...")

    addShutdownHook()

    startKoin(modules)
    setProperty("vertx-instance", vertx)

    vertx.registerVerticleFactory(KoinVerticleFactory)
    vertx.deployVerticleAwait(DataVerticle::class.koinName())
    vertx.deployVerticleAwait(HttpServerVerticle::class.koinName())

    logger.info("started")
  }

  override suspend fun stop() {
    logger.info("stopped")
  }

  private fun addShutdownHook() {

    Runtime.getRuntime().addShutdownHook(object : Thread() {
      override fun run() {
        logger.info("Undeploy all verticles")
        runBlocking { vertx.closeAwait() }
      }
    })
  }
}

val modules = listOf(dataModule, serverModule)
