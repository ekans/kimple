package org.ekans.kimple.server

import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.sockjs.BridgeOptions
import io.vertx.ext.web.handler.sockjs.SockJSHandler
import io.vertx.kotlin.core.http.listenAwait
import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.ekans.kimple.server.handlers.installHandlers
import org.koin.standalone.KoinComponent
import org.koin.standalone.get
import org.slf4j.LoggerFactory


private val logger = LoggerFactory.getLogger(HttpServerVerticle::class.java)

class HttpServerVerticle : CoroutineVerticle(), KoinComponent {

  override suspend fun start() {
    logger.info("Http server starting...")

    val router = get<Router>()
    router.installHandlers()

    val sockJSHandler = SockJSHandler.create(vertx)
    val options = BridgeOptions()
    sockJSHandler.bridge(options)
    router.route("/eventbus/*").handler(sockJSHandler)

    try {
      val server = vertx.createHttpServer()
        .requestHandler(router)
        .listenAwait(8080)

      logger.info("Http server port: ${server.actualPort()}")
      logger.info("Http server started")

    } catch (e: Throwable) {
      logger.error("Http server failed to start.", e)
    }
  }

  override suspend fun stop() {
    logger.info("Http server stopped.")
  }
}
