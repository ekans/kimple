import org.ekans.kimple.buildsrc.Librairies
import org.ekans.kimple.buildsrc.Modules
import org.ekans.kimple.buildsrc.Versions
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm")
  kotlin("kapt")
  id("io.vertx.vertx-plugin") version Plugin.Versions.VERTX
}

dependencies {
  implementation(project(Modules.DOMAIN))
  implementation(project(Modules.DATA))
  implementation(project(Modules.SERVER_API))

  kapt("io.vertx:vertx-codegen:3.6.3:processor")
  compileOnly("io.vertx:vertx-codegen:3.6.3")
  compile("io.vertx:vertx-service-proxy:3.6.3")

  implementation(Librairies.KOTLIN_STDLIB)
  implementation(Librairies.COROUTINES_CORE)
  implementation(Librairies.JACKSON_MODULE_KOTLIN)
  implementation(Librairies.KOIN_CORE)
  implementation(Librairies.LOGBACK_CLASSIC)
  implementation(Librairies.VERTX_CORE)
  implementation(Librairies.VERTX_WEB)
  implementation(Librairies.VERTX_LANG_KOTLIN)
  implementation(Librairies.VERTX_KOTLIN_COROUTINES)
}

vertx {
  mainVerticle = "org.ekans.kimple.server.BootstrapVerticle"
  version = Librairies.Versions.VERTX
}

tasks {
  withType(KotlinCompile::class) {
    kotlinOptions.jvmTarget = Versions.JVM_TARGET
  }
}
