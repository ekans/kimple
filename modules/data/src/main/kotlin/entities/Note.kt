package org.ekans.kimple.data.entities

import org.ekans.kimple.domain.entities.Note

data class Note(val id: String, val content: String) {

  companion object {
    fun fromDomain(note: Note): org.ekans.kimple.data.entities.Note {

      val id = note.id ?: System.nanoTime().toString()
      return org.ekans.kimple.data.entities.Note(id = id, content = note.content)
    }
  }

  fun toDomain() = Note(id = id, content = content)
}