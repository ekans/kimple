package org.ekans.kimple.data

import org.ekans.kimple.data.entities.Note

object Data {
  val notes = mutableListOf(Note(System.nanoTime().toString(), "from data repository"))
}
