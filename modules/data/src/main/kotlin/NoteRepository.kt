package org.ekans.kimple.data

import org.ekans.kimple.domain.entities.Note
import org.ekans.kimple.domain.usecases.CreateNoteUseCase
import org.ekans.kimple.domain.usecases.GetNotesUseCase

internal class NoteRepository : GetNotesUseCase, CreateNoteUseCase {

  override fun getNotes(): List<Note> {

    return Data.notes.map { it.toDomain() }
  }

  override fun createNote(note: Note): Note {

    val newNote = org.ekans.kimple.data.entities.Note.fromDomain(note)
    Data.notes.add(newNote)
    return newNote.toDomain()
  }
}