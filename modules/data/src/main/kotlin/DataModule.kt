package org.ekans.kimple.data

import org.ekans.kimple.domain.usecases.CreateNoteUseCase
import org.ekans.kimple.domain.usecases.GetNotesUseCase
import org.koin.dsl.module.module

val dataModule = module {
  single { NoteRepository() }
  single { get<NoteRepository>() as GetNotesUseCase }
  single { get<NoteRepository>() as CreateNoteUseCase }
}
