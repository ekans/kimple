plugins {
    kotlin("jvm") version Plugin.Versions.KOTLIN apply false
    id("com.github.ben-manes.versions") version Plugin.Versions.BEN_NAMES_VERSIONS
}

allprojects {

    repositories {
        mavenLocal()
        jcenter()
    }
}
